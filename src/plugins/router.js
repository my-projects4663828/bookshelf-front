import {createRouter, createWebHistory} from "vue-router";

const routes = [
    {
        path: '/',
        component: () => import("@/pages/HomePage.vue")
    },
    {
        path: '/categories/:id',
        component: () => import("@/pages/HomePage.vue")
    },
    {
        path: '/book-info/:bookId',
        component: () => import("@/pages/BookInfoPage.vue")
    },
    {
        path: '/book-add',
        component: () => import("@/pages/AddBook.vue")
    },
    {
        path: '/login',
        component: () => import("@/pages/LoginPage.vue")
    },
    {
        path: '/userCreate',
        component: () => import("@/pages/UserCreatePage.vue")
    }
]

export default createRouter({
    history: createWebHistory(),
    routes
})